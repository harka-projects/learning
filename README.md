# My Learning Journey

2025.03.12. Vue.js practice

2025.03.11. Vue.js practice

2025.03.10. Sport day

2025.03.09. Sport day

2025.03.08. Sport day

2025.03.07. Vue.js practice

2025.03.06. Vue.js practice

2025.03.05. Vue.js practice

2025.03.04. Vue.js practice

2025.03.03. Vue.js practice

2025.03.02. Sport day

2025.03.01. Sport day

2025.02.28. Vue.js practice

2025.02.27. Vue.js practice

2025.02.26. Vue.js practice

2025.02.25. Vue.js practice

2025.02.24. Vue.js practice

2025.02.23. Sport day

2025.02.22. Sport day

2025.02.21. Vue.js practice

2025.02.20. Vue.js practice

2025.02.19. Vue.js practice

2025.02.18. Vue.js practice

2025.02.17. Vue.js practice

2025.02.16. Sport day

2025.02.15. Sport day

2025.02.14. Vue.js practice

2025.02.13. Vue.js practice

2025.02.12. Vue.js practice

2025.02.11. Vue.js practice

2025.02.10. Vue.js practice

2025.02.09. Sport day

2025.02.08. Sport day

2025.02.07. Vue.js practice

2025.02.06. Vue.js practice

2025.02.05. Vue.js practice

2025.02.04. Vue.js practice

2025.02.03. Vue.js practice

2025.02.02. Sport day

2025.02.01. Sport day

2025.01.31. Vue.js practice

2025.01.30. Vue.js practice

2025.01.29. Vue.js practice

2025.01.28. Vue.js practice

2025.01.27. Vue.js practice

2025.01.26. Sport day

2025.01.25. Sport day

2025.01.24. Vue.js practice

2025.01.23. Vue.js practice

2025.01.22. Vue.js practice

2025.01.21. Vue.js practice

2025.01.20. Vue.js practice

2025.01.19. Sport day

2025.01.18. Sport day

2025.01.17. Vue.js practice

2025.01.16. Vue.js practice

2025.01.15. Vue.js practice

2025.01.14. Vue.js practice

2025.01.13. Vue.js practice

2025.01.12. Sport day

2025.01.11. Sport day

2025.01.10. Vue.js practice

2025.01.09. Vue.js practice

2025.01.08. Vue.js practice

2025.01.07. Vue.js practice

2025.01.06. Vue.js practice

2025.01.05. Sport day

2025.01.04. Sport day

2025.01.03. Vue.js practice

2025.01.02. Vue.js practice

2025.01.01. Rest day

2024.12.31. Rest day

2024.12.30. AWS practice

2024.12.29. AWS practice

2024.12.28. AWS practice

2024.12.27. Rest day

2024.12.26. Rest day

2024.12.25. Rest day

2024.12.24. Rest day

2024.12.23. Rest day

2024.12.22. Rest day

2024.12.21. Rest day

2024.12.20. Rest day

2024.12.19. Vue.js practice

2024.12.18. Vue.js practice

2024.12.17. Vue.js practice

2024.12.16. Vue.js practice

2024.12.15. Sport day

2024.12.14. Sport day

2024.12.13. Vue.js practice

2024.12.12. Vue.js practice

2024.12.11. Vue.js practice

2024.12.10. Vue.js practice

2024.12.09. Vue.js practice

2024.12.08. Sport day

2024.12.07. Sport day

2024.12.06. Vue.js practice

2024.12.05. Vue.js practice

2024.12.04. AWS practice

2024.12.03. AWS practice

2024.12.02. Sport day

2024.12.01. Sport day

2024.11.30. Bun, JavaScript practice

2024.11.29. Bun, JavaScript practice

2024.11.28. Vue.js practice

2024.11.27. Vue.js practice

2024.11.26. Vue.js practice

2024.11.25. Vue.js practice

2024.11.24. Sport day

2024.11.23. Sport day

2024.11.22. Vue.js practice

2024.11.21. Vue.js practice

2024.11.20. Vue.js practice

2024.11.19. Vue.js practice

2024.11.18. Vue.js practice

2024.11.17. Sport day

2024.11.16. Sport day

2024.11.15. Vue.js practice

2024.11.14. Vue.js practice

2024.11.13. Vue.js practice

2024.11.12. Vue.js practice

2024.11.11. Vue.js practice

2024.11.10. Sport day

2024.11.09. Sport day

2024.11.08. Vue.js practice

2024.11.07. Vue.js practice

2024.11.06. Vue.js practice

2024.11.05. Vue.js practice

2024.11.04. Vue.js practice

2024.11.03. Vue.js practice

2024.11.02. Sport day

2024.11.01. Sport day

2024.10.31. Vue.js practice

2024.10.30. Vue.js practice

2024.10.29. Vue.js practice

2024.10.28. Vue.js practice

2024.10.27. Sport day

2024.10.26. Sport day

2024.10.25. Vue.js practice

2024.10.24. Vue.js practice

2024.10.23. Vue.js practice

2024.10.22. Vue.js practice

2024.10.21. Vue.js practice

2024.10.20. Sport day

2024.10.19. Sport day

2024.10.18. Vue.js practice

2024.10.17. Vue.js practice

2024.10.16. Vue.js practice

2024.10.15. Vue.js practice

2024.10.14. Vue.js practice

2024.10.13. Sport day

2024.10.12. Sport day

2024.10.11. Vue.js practice

2024.10.10. Vue.js practice

2024.10.09. Vue.js practice

2024.10.08. Vue.js practice

2024.10.07. Vue.js practice

2024.10.06. Sport day

2024.10.05. Sport day

2024.10.04. Vue.js practice

2024.10.03. Vue.js practice

2024.10.02. Vue.js practice

2024.10.01. Vue.js practice

2024.09.30. Vue.js practice

2024.09.29. Sport day

2024.09.28. Sport day

2024.09.27. Vue.js practice

2024.09.26. Vue.js practice

2024.09.25. Vue.js practice

2024.09.24. Vue.js practice

2024.09.23. Vue.js practice

2024.09.22. Sport day

2024.09.21. Sport day

2024.09.20. Sport day

2024.09.19. Vue.js practice

2024.09.18. Vue.js practice

2024.09.17. Vue.js practice

2024.09.16. Vue.js practice

2024.09.15. Sport day

2024.09.14. Sport day

2024.09.13. Vue.js practice

2024.09.12. Vue.js practice

2024.09.11. Vue.js practice

2024.09.10. Vue.js practice

2024.09.09. Vue.js practice

2024.09.08. Sport day

2024.09.07. Sport day

2024.09.06. Vue.js practice

2024.09.05. Vue.js practice

2024.09.04. Vue.js practice

2024.09.03. Vue.js practice

2024.09.02. Vue.js practice

2024.09.01. Sport day

2024.08.31. Sport day

2024.08.30. Vue.js practice

2024.08.29. Vue.js practice

2024.08.28. Vue.js practice

2024.08.27. Vue.js practice

2024.08.26. Vue.js practice

2024.08.25. Sport day

2024.08.24. Sport day

2024.08.23. Vue.js practice

2024.08.22. Vue.js practice

2024.08.21. Vue.js practice

2024.08.20. Vue.js practice

2024.08.19. Vue.js practice

2024.08.18. Sport day

2024.08.17. Sport day

2024.08.16. Vue.js practice

2024.08.15. Sport day

2024.08.14. Vue.js practice

2024.08.13. Vue.js practice

2024.08.12. Vue.js practice

2024.08.11. Sport day

2024.08.10. Sport day

2024.08.09. Vue.js practice

2024.08.08. Vue.js practice

2024.08.07. Vue.js practice

2024.08.06. Vue.js practice

2024.08.05. Vue.js practice

2024.08.04. Sport day

2024.08.03. Sport day

2024.08.02. Sport day

2024.08.01. Vue.js practice

2024.07.31. Sport day

2024.07.30. Vue.js practice

2024.07.29. Vue.js practice

2024.07.28. Sport day

2024.07.27. Sport day

2024.07.26. Vue.js practice

2024.07.25. Vue.js practice

2024.07.24. Sport day

2024.07.23. Vue.js practice

2024.07.22. Vue.js practice

2024.07.21. Sport day

2024.07.20. Sport day

2024.07.19. Vue.js practice

2024.07.18. Vue.js practice

2024.07.17. Sport day

2024.07.16. Vue.js practice

2024.07.15. Vue.js practice

2024.07.14. Sport day

2024.07.13. Sport day

2024.07.12. Vue.js practice

2024.07.11. Vue.js practice

2024.07.10. Vue.js practice

2024.07.09. Vue.js practice

2024.07.08. Vue.js practice

2024.07.07. Sport day

2024.07.06. Sport day

2024.07.05. Vue.js practice

2024.07.04. Vue.js practice

2024.07.03. Vue.js practice

2024.07.02. WordPress practice

2024.07.01. Sport day

2024.06.30. Sport day

2024.06.29. Sport day

2024.06.28. Sport day

2024.06.27. Sport day

2024.06.26. Vue.js practice

2024.06.25. Vue.js practice

2024.06.24. Vue.js practice

2024.06.23. Sport day

2024.06.22. Sport day

2024.06.21. Vue.js practice

2024.06.20. Vue.js practice

2024.06.19. Sport day

2024.06.18. Vue.js practice

2024.06.17. Vue.js practice

2024.06.16. Sport day

2024.06.15. Sport day

2024.06.14. Sport day

2024.06.13. Vue.js practice

2024.06.12. Vue.js practice

2024.06.11. Vue.js practice

2024.06.10. Vue.js practice

2024.06.09. WordPress practice

2024.06.08. WordPress practice

2024.06.07. WordPress practice

2024.06.06. Vue.js practice

2024.06.05. Vue.js practice

2024.06.04. WordPress practice

2024.06.03. Vue.js practice

2024.06.02. WordPress practice

2024.06.01. WordPress practice

2024.05.31. Vue.js practice

2024.05.30. Vue.js practice

2024.05.29. Vue.js practice

2024.05.28. Vue.js practice

2024.05.27. Vue.js practice

2024.05.26. WordPress practice

2024.05.25. WordPress practice

2024.05.24. Vue.js practice

2024.05.23. Vue.js practice

2024.05.22. WordPress practice

2024.05.21. Vue.js practice

2024.05.20. WordPress practice

2024.05.19. WordPress practice

2024.05.18. WordPress practice

2024.05.17. AWS learning & Vue.js practice

2024.05.16. AWS learning & Vue.js practice

2024.05.15. AWS learning & Vue.js practice

2024.05.14. AWS learning & Vue.js practice

2024.05.13. AWS learning & Vue.js practice

2024.05.12. WordPress practice

2024.05.11. WordPress practice

2024.05.10. AWS learning & Vue.js practice

2024.05.09. WordPress practice

2024.05.08. AWS learning & Vue.js practice

2024.05.07. AWS learning & Vue.js practice

2024.05.06. AWS learning & Vue.js practice

2024.05.05. WordPress practice

2024.05.04. WordPress practice

2024.05.03. AWS learning & Vue.js practice

2024.05.02. AWS learning & Vue.js practice

2024.05.01. WordPress practice

2024.04.30. AWS learning & Vue.js practice

2024.04.29. AWS learning & Vue.js practice

2024.04.28. Sport day

2024.04.27. Sport day

2024.04.26. AWS learning & Vue.js practice

2024.04.25. AWS learning & Vue.js practice

2024.04.24. AWS learning & Vue.js practice

2024.04.23. AWS learning & Vue.js practice

2024.04.22. AWS learning & Vue.js practice

2024.04.21. Sport day

2024.04.20. Sport day

2024.04.19. AWS learning & Vue.js practice

2024.04.18. AWS learning & Vue.js practice

2024.04.17. AWS learning & Vue.js practice

2024.04.16. AWS learning & Vue.js practice

2024.04.15. AWS learning & Vue.js practice

2024.04.14. Sport day

2024.04.13. Sport day

2024.04.12. AWS learning & Vue.js practice

2024.04.11. AWS learning & Vue.js practice

2024.04.10. AWS learning & Vue.js practice

2024.04.09. AWS learning & Vue.js practice

2024.04.08. AWS learning & Vue.js practice

2024.04.07. Sport day

2024.04.06. Sport day

2024.04.05. AWS learning & Vue.js practice

2024.04.04. AWS learning & Vue.js practice

2024.04.03. AWS learning & Vue.js practice

2024.04.02. AWS learning & Vue.js practice

2024.04.01. Sport day

2024.03.31. Sport day

2024.03.30. Sport day

2024.03.29. AWS learning & Vue.js practice

2024.03.28. AWS learning & Vue.js practice

2024.03.27. AWS learning & Vue.js practice

2024.03.26. AWS learning & Vue.js practice

2024.03.25. AWS learning & Vue.js practice

2024.03.24. Sport day

2024.03.23. Sport day

2024.03.22. AWS learning & Vue.js practice

2024.03.21. AWS learning & Vue.js practice

2024.03.20. AWS learning & Vue.js practice

2024.03.19. AWS learning & Vue.js practice

2024.03.18. AWS learning & Vue.js practice

2024.03.17. Sport day

2024.03.16. Sport day

2024.03.15. AWS learning & Vue.js practice

2024.03.14. AWS learning & Vue.js practice

2024.03.13. AWS learning & Vue.js practice

2024.03.12. AWS learning & Vue.js practice

2024.03.11. AWS learning & Vue.js practice

2024.03.10. Sport day

2024.03.09. Sport day

2024.03.08. AWS learning & Vue.js practice

2024.03.07. AWS learning & Vue.js practice

2024.03.06. AWS learning & Vue.js practice

2024.03.05. AWS learning & Vue.js practice

2024.03.04. AWS learning & Vue.js practice

2024.03.03. Sport day

2024.03.02. Sport day

2024.03.01. AWS learning & Vue.js practice

2024.02.29. AWS learning & Vue.js practice

2024.02.28. AWS learning & Vue.js practice

2024.02.27. AWS learning & Vue.js practice

2024.02.26. AWS learning & Vue.js practice

2024.02.25. Sport day

2024.02.24. Sport day

2024.02.23. AWS learning & Vue.js practice

2024.02.22. AWS learning & Vue.js practice

2024.02.21. AWS learning & Vue.js practice

2024.02.20. AWS learning & Vue.js practice

2024.02.19. AWS learning & Vue.js practice

2024.02.18. Sport day

2024.02.17. Sport day

2024.02.16. Vue.js practice

2024.02.15. Vue.js practice

2024.02.14. Vue.js practice

2024.02.13. Vue.js practice

2024.02.12. Vue.js practice

2024.02.11. Sport day

2024.02.10. Sport day

2024.02.09. Vue.js practice

2024.02.08. Vue.js practice

2024.02.07. Vue.js practice

2024.02.06. Vue.js practice

2024.02.05. Vue.js practice

2024.02.04. Sport day

2024.02.03. Sport day

2024.02.02. Vue.js practice

2024.02.01. Vue.js practice

2024.01.31. Vue.js practice

2024.01.30. Vue.js practice

2024.01.29. Vue.js practice

2024.01.28. Sport day

2024.01.27. Sport day

2024.01.26. Vue.js practice

2024.01.25. Vue.js practice

2024.01.24. Vue.js practice

2024.01.23. Vue.js practice

2024.01.22. Vue.js practice

2024.01.21. Sport day

2024.01.20. Sport day

2024.01.19. Vue.js practice

2024.01.18. Vue.js practice

2024.01.17. Vue.js practice

2024.01.16. Vue.js practice

2024.01.15. Vue.js practice

2024.01.14. Sport day

2024.01.13. Sport day

2024.01.12. Vue.js practice

2024.01.11. Vue.js practice

2024.01.10. Vue.js practice

2024.01.09. Vue.js practice

2024.01.08. Vue.js practice

2024.01.07. Sport day

2024.01.06. Sport day

2024.01.05. Vue.js practice

2024.01.04. Vue.js practice

2024.01.03. Vue.js practice

2024.01.02. Vue.js practice

2024.01.01. Vue.js practice

2023.12.31. Nuxt practice

2023.12.30. Nuxt practice

2023.12.29. Nuxt practice

2023.12.28. Nuxt practice

2023.12.27. Rest day

2023.12.26. Rest day

2023.12.25. Rest day

2023.12.24. Rest day

2023.12.23. Nuxt practice

2023.12.22. Nuxt practice

2023.12.21. Nuxt practice

2023.12.20. Nuxt practice

2023.12.19. Nuxt practice

2023.12.18. Nuxt practice

2023.12.17. Sport day

2023.12.16. Sport day

2023.12.15. Nuxt practice

2023.12.14. Nuxt practice

2023.12.13. Sport day

2023.12.12. Nuxt practice

2023.12.11. Nuxt practice

2023.12.10. Sport day

2023.12.09. Sport day

2023.12.08. Sport day

2023.12.07. Nuxt practice

2023.12.06. Vue & Node.js practice

2023.12.05. Vue & Node.js practice

2023.12.04. Vue & Node.js practice

2023.12.03. Sport day

2023.12.02. Sport day

2023.12.01. Vue & Node.js practice

2023.11.30. Vue & Node.js practice

2023.11.29. Vue & Node.js practice

2023.11.28. Vue & Node.js practice

2023.11.27. Vue & Node.js practice

2023.11.26. Sport day

2023.11.25. Sport day

2023.11.24. Bun & Vue practice

2023.11.23. Vue practice

2023.11.22. Vue practice

2023.11.21. Vue practice

2023.11.20. Vue practice

2023.11.19. Sport day

2023.11.18. Sport day

2023.11.17. Vue practice

2023.11.16. Vue practice

2023.11.15. Vue practice

2023.11.14. Vue practice

2023.11.13. Vue practice

2023.11.12. Sport day

2023.11.11. Sport day

2023.11.10. Vue practice

2023.11.09. Vue practice

2023.11.08. Vue practice

2023.11.07. Vue practice

2023.11.06. Nginx & Lua practice

2023.11.05. Nginx & Lua practice

2023.11.04. Nginx & Lua practice

2023.11.03. Vue practice

2023.11.02. Vue practice

2023.11.01. Sport day

2023.10.31. Vue practice

2023.10.30. Vue practice

2023.10.29. Sport day

2023.10.28. Sport day

2023.10.27. Vue practice

2023.10.26. Vue practice

2023.10.25. Vue practice

2023.10.24. Vue practice

2023.10.23. Vue practice

2023.10.22. Sport day

2023.10.21. Sport day

2023.10.20. Vue practice

2023.10.19. Vue practice

2023.10.18. Vue practice

2023.10.17. Vue practice

2023.10.16. Vue practice

2023.10.15. Sport day

2023.10.14. Sport day

2023.10.13. Vue practice

2023.10.12. Vue practice

2023.10.11. Vue practice

2023.10.10. Vue practice

2023.10.09. Vue practice

2023.10.08. Nuxt & Vue practice

2023.10.07. Nuxt & Vue practice

2023.10.06. Vue practice

2023.10.05. Vue practice

2023.10.04. Vue practice

2023.10.03. Vue practice

2023.10.02. Vue practice

2023.10.01. Sport day

2023.09.30. Sport day

2023.09.29. Vue practice

2023.09.28. Vue practice

2023.09.27. Vue practice

2023.09.26. Vue practice

2023.09.25. Vue practice

2023.09.24. Nuxt & Vue practice

2023.09.23. Nuxt & Vue practice

2023.09.22. Vue practice

2023.09.21. Vue practice

2023.09.20. Vue practice

2023.09.19. Vue practice

2023.09.18. Vue practice

2023.09.17. Sport day

2023.09.16. Sport day

2023.09.15. Vue practice

2023.09.14. Vue practice

2023.09.13. Vue practice

2023.09.12. Vue practice

2023.09.11. Vue practice

2023.09.10. Sport day

2023.09.09. Sport day

2023.09.08. Vue practice

2023.09.07. Vue practice

2023.09.06. Vue practice

2023.09.05. Vue practice

2023.09.04. Vue practice

2023.09.03. Sport day

2023.09.02. Sport day

2023.09.01. Vue practice

2023.08.31. Vue practice

2023.08.30. Vue practice

2023.08.29. Vue practice

2023.08.28. Vue practice

2023.08.27. Sport day

2023.08.26. Sport day

2023.08.25. Vue practice

2023.08.24. Vue practice

2023.08.23. Vue practice

2023.08.22. Vue practice

2023.08.21. Vue practice

2023.08.20. Sport day

2023.08.19. Sport day

2023.08.18. Vue practice

2023.08.17. Vue practice

2023.08.16. Vue practice

2023.08.15. Sport day

2023.08.14. Sport day

2023.08.13. Sport day

2023.08.12. Sport day

2023.08.11. Vue practice

2023.08.10. Nuxt & Vue practice

2023.08.09. Nuxt & Vue practice

2023.08.08. Nuxt & Vue practice

2023.08.07. Nuxt & Vue practice

2023.08.06. Nuxt & Vue practice

2023.08.05. Sport day

2023.08.04. Sport day

2023.08.03. Vue practice

2023.08.02. Vue practice

2023.08.01. Vue practice

2023.07.31. Vue practice

2023.07.30. Sport day

2023.07.29. Sport day

2023.07.28. Vue practice

2023.07.27. Vue practice

2023.07.26. Linux & NGINX practice

2023.07.25. JavaScript practice

2023.07.24. Linux & NGINX practice

2023.07.23. Linux & NGINX practice

2023.07.22. Linux & NGINX practice

2023.07.21. Linux & NGINX practice

2023.07.20. Linux & NGINX practice

2023.07.17. Linux & NGINX practice

2023.07.16. GitLab learning

2023.07.15. Sport day

2023.07.14. Linux & NGINX practice

2023.07.13. Linux & NGINX practice

2023.07.12. Linux & NGINX practice

2023.07.11. Vue practice

2023.07.10. Vue practice

2023.07.09. Sport day

2023.07.08. Sport day

2023.07.07. Bash scripting practice

2023.07.06. Vue practice

2023.07.05. Vue practice

2023.07.04. Vue practice

2023.07.03. Vue practice

2023.07.02. Sport day

2023.07.01. Sport day

2023.06.30. Vue practice

2023.06.29. WSL & Docker practice

2023.06.28. WSL & Docker practice

2023.06.27. WSL & Docker practice

2023.06.26. WSL & Docker practice

2023.06.25. Sport day

2023.06.24. Sport day

2023.06.23. Vue learning

2023.06.22. Vue learning

2023.06.21. Vue learning

2023.06.20. TDD practice

2023.06.19. Vue learning

2023.06.18. Sport day

2023.06.17. Sport day

2023.06.16. Vue learning

2023.06.15. Vue learning

2023.06.14. Vue learning

2023.06.13. Vue learning

2023.06.12. Vue learning

2023.06.10. Vue learning

2023.06.09. Vue learning

2023.06.08. Vue learning

2023.06.07. Vue learning

2023.06.06. Vue learning

2023.06.05. Vue learning

2023.06.04. Sport day

2023.06.03. Sport day

2023.06.02. JavaScript practice

2023.06.01. JavaScript practice

2023.05.31. REST API practice

2023.05.30. REST API practice

2023.05.26. JavaScript practice

2023.05.24. TDD learning

2023.05.23. TDD learning

2023.05.17. GraphQL learning

2023.05.16. Node.js practice

2023.05.15. SQLite learning

2023.05.14. e-Health Hackathon (Product: [Mentally](https://mentally.harka.com))

2023.05.13. e-Health Hackathon (Product: [Mentally](https://mentally.harka.com))

2023.05.12. e-Health Hackathon (Product: [Mentally](https://mentally.harka.com))

2023.05.11. MySQL learning

2023.05.10. MySQL learning

2023.05.09. MySQL learning

2023.05.08. MySQL learning

2023.05.07. MySQL learning

2023.05.06. Sport day

2023.05.05. MySQL learning

2023.05.04. MySQL learning

2023.05.03. MySQL learning

2023.04.28. JavaScript practice

2023.04.27. Test Driven Development learning

2023.04.26. Test Driven Development learning

2023.04.25. Test Driven Development learning

2023.04.24. Test Driven Development learning

2023.04.19. Unit Testing learning

2023.04.18. Unit Testing learning

2023.04.17. OOP learning

2023.04.16. OOP learning

2023.04.13. Refactoring learning

2023.04.12. JavaScript OOP practice

2023.04.09. JavaScript OOP practice

2023.04.08. JavaScript OOP practice

2023.04.05. JavaScript OOP practice

2023.04.04. JavaScript OOP practice

2023.03.30. JavaScript practice

2023.03.28. Test Driver Development learning

2023.03.27. JavaScript practice

2023.03.23. JavaScript practice

2023.03.22. JavaScript practice

2023.03.21. GitLab learning

2023.03.20. Docker learning

2023.03.19. SCSS practice

2023.03.18. Sport day

2023.03.17. Sport day

2023.03.16. SCSS practice

2023.03.15. SCSS practice

2023.03.14. SCSS practice

2023.03.13. SCSS practice

2023.03.12. Sport day

2023.03.11. Sport day

2023.03.10. SCSS practice

2023.03.09. SCSS practice

2023.03.08. SCSS practice

2023.03.07. JavaScript practice

2023.03.06. JavaScript practice

2023.03.05. CSS practice

2023.03.04. CSS practice

2023.03.03. Docker learning

2023.03.02. Docker learning

2023.03.01. Docker learning

2023.02.28. Sport day

2023.02.27. Sport day

2023.02.26. JavaScript practice

2023.02.25. Sport day

2023.02.24. JavaScript practice

2023.02.23. Node.js practice

2023.02.22. Node.js practice

2023.02.21. JavaScript practice

2023.02.20. JavaScript practice

2023.02.19. Sport day

2023.02.18. Sport day

2023.02.17. JavaScript practice

2023.02.16. JavaScript practice

2023.02.15. Node.js practice

2023.02.14. Node.js practice

2023.02.13. Linux learning

2023.02.12. Sport day

2023.02.11. Sport day

2023.02.10. Linux learning

2023.02.09. Node.js practice

2023.02.08. Node.js practice

2023.02.07. JavaScript practice

2023.02.06. JavaScript practice

2023.02.05. Sport day

2023.02.04. Sport day

2023.02.03. Linux learning

2023.02.02. Linux learning

2023.02.01. Linux learning

2023.01.31. Linux learning

2023.01.30. Sport day

2023.01.29. Linux learning

2023.01.28. Node.js practice

2023.01.27. Sport day

2023.01.26. Sport day

2023.01.25. Sport day

2023.01.24. Node.js practice

2023.01.23. Node.js & JavaScript practice

2023.01.22. Node.js & JavaScript practice

2023.01.21. Sport day

2023.01.20. Node.js & JavaScript practice

2023.01.19. Node.js & JavaScript practice

2023.01.18. Bootstrap & SCSS practice

2023.01.17. Bootstrap & SCSS practice

2023.01.16. Node.js & JavaScript practice

2023.01.15. Node.js & JavaScript practice

2023.01.14. Node.js & JavaScript practice

2023.01.13. Sport day

2023.01.12. Node.js & JavaScript practice

2023.01.11. Node.js & JavaScript practice

2023.01.10. Node.js & JavaScript practice

2023.01.09. JavaScript practice

2023.01.08. JavaScript practice

2023.01.07. Sport day

2023.01.06. JavaScript practice

2023.01.05. JavaScript practice

2023.01.04. JavaScript practice

2023.01.03. JavaScript practice

2023.01.02. MongoDB & Mongoose learning

2023.01.01. Sport day

2022.12.31. Sport day

2022.12.30. MongoDB & Mongoose learning

2022.12.29. MongoDB & Mongoose learning

2022.12.28. MongoDB & Mongoose learning

2022.12.27. MongoDB & Mongoose learning

2022.12.26. Node.js practice

2022.12.25. Sport day

2022.12.24. Sport day

2022.12.23. HTTP requests practice

2022.12.22. JavaScript practice

2022.12.21. JavaScript practice

2022.12.20. JavaScript practice

2022.12.19. JavaScript practice

2022.12.18. JavaScript practice

2022.12.17. Sport day

2022.12.16. jQuery & JavaScript practice

2022.12.15. jQuery & JavaScript practice

2022.12.14. jQuery & JavaScript practice

2022.12.13. jQuery & JavaScript practice

2022.12.12. jQuery & JavaScript practice

2022.12.11. Sport day

2022.12.10. JavaScript practice

2022.12.09. jQuery & JavaScript practice

2022.12.08. jQuery & JavaScript practice

2022.12.07. jQuery practice

2022.12.06. jQuery practice

2022.12.05. Angular, TypeScript and JavaScript practice

2022.12.04. Angular, TypeScript and JavaScript practice

2022.12.03. Sport day

2022.12.02. Node.js practice

2022.12.01. Shopify Liquid practice

2022.11.30. Bootstrap practice

2022.11.29. Bootstrap practice

2022.11.28. Bootstrap and Shopify Liquid practice

2022.11.27. Sport day

2022.11.26. Domains and Cloudflare learning

2022.11.25. PHP & Bootstrap practice

2022.11.24. PHP & Bootstrap practice

2022.11.23. PHP & Bootstrap practice

2022.11.22. PHP & Bootstrap practice

2022.11.21. PHP & Bootstrap practice

2022.11.20. NGINX & Node.js learning

2022.11.19. Sport day

2022.11.18. PHP & Bootstrap practice

2022.11.17. PHP & Bootstrap practice

2022.11.16. PHP & Bootstrap practice

2022.11.15. Sport day

2022.11.14. Bootstrap practice

2022.11.13. PHP practice

2022.11.12. JavaScript practice

2022.11.11. JavaScript practice

2022.11.10. Chrome Web Store Extension learning

2022.11.09. NGINX learning

2022.11.08. Bootstrap learning

2022.11.07. NGINX learning

2022.11.06. Node.js learning

2022.11.05. Sport day

2022.11.04. Sport day

2022.11.03. NGINX learning

2022.11.02. NGINX learning

2022.11.01. JavaScript practice

2022.10.31. NGINX learning

2022.10.30. NGINX learning

2022.10.29. Sport day

2022.10.28. Sport day

2022.10.27. JavaScript & Shopify practice

2022.10.26. Shopify practice

2022.10.25. WordPress and Shopify practice

2022.10.24. WordPress & JavaScript practice

2022.10.23. Sport day

2022.10.22. Bash and Node.js practice

2022.10.21. Bash and Node.js practice

2022.10.20. Bootstrap & JavaScript practice

2022.10.19. Bootstrap & JavaScript practice

2022.10.18. Bootstrap & JavaScript practice

2022.10.17. Bootstrap & JavaScript practice

2022.10.16. Sport day

2022.10.15. Node.js & JavaScript practice

2022.10.14. Node.js & JavaScript practice

2022.10.13. Bootstrap & JavaScript practice

2022.10.12. Bootstrap & JavaScript practice

2022.10.11. Bootstrap & JavaScript practice

2022.10.10. Bootstrap & JavaScript practice

2022.10.09. Sport day

2022.10.08. JavaScript practice

2022.10.07. Bootstrap & JavaScript practice

2022.10.06. Database & JavaScript practice

2022.10.05. PHP & JavaScript practice

2022.10.04. PHP & JavaScript practice

2022.10.03. PHP & JavaScript practice

2022.10.02. JavaScript practice

2022.10.01. Sport day

2022.09.30. PHP & JavaScript practice

2022.09.29. PHP & JavaScript practice

2022.09.28. PHP & WordPress practice

2022.09.27. PHP & WordPress practice

2022.09.26. PHP & WordPress practice

2022.09.25. Sport day

2022.09.24. JavaScript practice

2022.09.23. PHP and WordPress practice

2022.09.22. PHP and WordPress practice

2022.09.21. PHP and JavaScript practice

2022.09.20. PHP and WordPress practice

2022.09.19. Bootstrap practice

2022.09.18. Sport day

2022.09.17. JavaScript practice

2022.09.16. JavaScript and Bootstrap practice

2022.09.15. JavaScript and Bootstrap practice

2022.09.14. JavaScript and Bootstrap practice

2022.09.13. JavaScript and Bootstrap practice

2022.09.12. JavaScript and Bootstrap practice

2022.08.11. Sport day

2022.08.10. Sport day

2022.08.09. Sport day

2022.09.08. JavaScript practice

2022.09.07. JavaScript practice

2022.08.06. Sport day

2022.08.05. Sport day

2022.09.04. JavaScript practice

2022.09.03. JavaScript practice

2022.09.02. JavaScript practice

2022.09.01. JavaScript practice

2022.08.31. Sport day

2022.08.30. JavaScript practice

2022.08.29. Sport day

2022.08.28. Sport day

2022.08.27. JavaScript practice

2022.08.26. Sport day

2022.08.25. Sport day

2022.08.24. Sport day

2022.08.23. JavaScript practice

2022.08.22. JavaScript and Bootstrap practice

2022.08.21. Sport day

2022.08.20. JavaScript practice

2022.08.19. CSS practice

2022.08.18. Bootstrap practice

2022.08.17. JavaScript and Bootstrap practice

2022.08.16. JavaScript and Bootstrap practice

2022.08.15. JavaScript practice

2022.08.14. JavaScript practice

2022.08.13. Sport day

2022.08.12. Bootstrap and JavaScript practice

2022.08.11. Bootstrap practice

2022.08.10. Bootstrap practice

2022.08.09. Bootstrap practice

2022.08.08. Bootstrap practice

2022.08.07. Sport day

2022.08.06. JavaScript practice

2022.08.05. Bootstrap practice

2022.08.04. Bootstrap practice

2022.08.03. Bootstrap practice

2022.08.02. Bootstrap and Shopify practice

2022.08.01. Bootstrap and Shopify practice

2022.07.31. Sport day

2022.07.30. Bootstrap and Shopify practice

2022.07.29. Bootstrap and Shopify practice

2022.07.28. Bootstrap and Shopify practice

2022.07.27. Bootstrap practice

2022.07.26. Bootstrap practice

2022.07.25. Bootstrap practice

2022.07.24. Sport day

2022.07.23. JavaScript practice

2022.07.22. Bootstrap practice

2022.07.21. Bootstrap practice

2022.07.20. Bootstrap & JavaScript practice

2022.07.19. Bootstrap & JavaScript practice

2022.07.18. Bootstrap & JavaScript practice

2022.07.17. Sport day

2022.07.16. JavaScript practice

2022.07.15. Bootstrap practice

2022.07.14. Bootstrap practice

2022.07.13. Bootstrap practice

2022.07.12. Bootstrap practice

2022.07.11. Bootstrap practice

2022.07.10. Bootstrap practice

2022.07.09. Sport day

2022.07.08. Sport day

2022.07.07. Bootstrap practice

2022.07.06. Bootstrap practice

2022.07.05. Bootstrap practice

2022.07.04. Bootstrap practice

2022.07.03. Bootstrap practice

2022.07.02. Sport day

2022.07.01. Bootstrap practice

2022.06.30. Bootstrap practice

2022.06.29. Bootstrap practice

2022.06.28. Bootstrap practice

2022.06.27. Bootstrap practice

2022.06.26. Bootstrap practice

2022.06.25. Sport day

2022.06.24. Bootstrap practice

2022.06.23. Bootstrap practice

2022.06.22. Shopify & Bootstrap practice

2022.06.21. Shopify & Bootstrap practice

2022.06.20. Shopify & Bootstrap practice

2022.06.19. Sport day

2022.06.18. Bootstrap practice

2022.06.17. Bootstrap practice

2022.06.16. Bootstrap practice

2022.06.15. Shopify practice

2022.06.14. Shopify practice

2022.06.13. Shopify practice

2022.06.12. Shopify practice

2022.06.11. Sport day

2022.06.10. EJS and Shopify practice

2022.06.09. Bootstrap and EJS practice

2022.06.08. Google Maps API and Axios practice

2022.06.07. EJS and Axios practice

2022.06.06. EJS and Bootstrap practice

2022.06.05. Sport day

2022.06.04. Sport day

2022.06.03. EJS and Bootstrap practice

2022.06.02. EJS and Bootstrap practice

2022.06.01. EJS and Bootstrap practice

2022.05.31. EJS and Bootstrap practice

2022.05.30. EJS and Bootstrap practice

2022.05.29. Angular practice

2022.05.28. Sport day

2022.05.27. EJS & Bootstrap practice

2022.05.26. Bootstrap practice

2022.05.25. Bootstrap practice

2022.05.24. Bootstrap practice

2022.05.23. Axios and Bootstrap learning

2022.05.22. Angular learning

2022.05.21. Sport day

2022.05.20. Bootstrap practice

2022.05.19. Bootstrap practice

2022.05.18. HTML & CSS practice

2022.05.17. HTML & CSS practice

2022.05.16. HTML & CSS practice

2022.05.15. Angular learning

2022.05.14. Sport day

2022.05.13. HTML & CSS practice

2022.05.12. HTML & CSS practice

2022.05.11. HTML & CSS practice

2022.05.10. HTML & CSS practice

2022.05.09. HTML & CSS practice

2022.05.08. Node.js learning

2022.05.07. Sport day

2022.05.06. Shopify learning

2022.05.05. Shopify learning

2022.05.04. Shopify learning

2022.05.03. Shopify learning

2022.05.02. Shopify learning

2022.05.01. Sport day

2022.04.30. Node.js learning

2022.04.29. Shopify learning

2022.04.28. Shopify learning

2022.04.27. Shopify learning

2022.04.26. Shopify learning

2022.04.25. Shopify learning

2022.04.24. Node.js learning

2022.04.23. Sport day

2022.04.22. Shopify learning

2022.04.21. Shopify learning

2022.04.20. Shopify learning

2022.04.19. Shopify learning

2022.04.18. Node.js learning

2022.04.17. Heroku learning

2022.04.16. Heroku learning

2022.04.15. Sport day

2022.04.14. CSS practice

2022.04.13. Shopify learning

2022.04.12. Shopify learning

2022.04.11. CSS practice

2022.04.10. Firebase learning

2022.04.09. Sport day

2022.04.08. Shopify learning

2022.04.07. CSS practice

2022.04.06. Shopify learning

2022.04.05. Firebase learning

2022.04.04. Shopify learning

2022.04.03. Sport day

2022.04.02. Node.js learning

2022.04.01. Shopify learning

2022.03.31. Shopify learning

2022.03.30. CSS practice

2022.03.29. CSS practice

2022.03.28. CSS practice

2022.03.27. Sport day

2022.03.26. Shopify learning

2022.03.25. Shopify learning

2022.03.24. Shopify learning

2022.03.23. Shopify learning

2022.03.22. Shopify learning

2022.03.21. Shopify learning

2022.03.20. Sport day

2022.03.19. NodeJS and MySQL learning

2022.03.18. Shopify learning

2022.03.17. Shopify learning

2022.03.16. Shopify learning

2022.03.15. NodeJS and Angular practice

2022.03.14. Sport day

2022.03.13. NodeJS and Angular practice

2022.03.12. NodeJS and Angular practice

2022.03.11. Shopify learning

2022.03.10. Shopify learning

2022.03.09. Shopify learning

2022.03.08. Shopify learning

2022.03.07. Shopify learning

2022.03.06. NodeJS and MySQL learning

2022.03.05. NodeJS and MySQL learning

2022.03.04. NodeJS and MySQL learning

2022.03.03. NodeJS and MySQL learning

2022.03.02. Sport day

2022.03.01. NodeJS and MySQL learning

2022.02.28. NodeJS and MySQL learning

2022.02.27. Angular and NodeJS learning

2022.02.26. Angular and NodeJS learning

2022.02.25. Sport day

2022.02.24. Angular and NodeJS learning

2022.02.23. NodeJS and MySQL learning

2022.02.22. Angular and NodeJS learning

2022.02.21. Angular and NodeJS learning

2022.02.20. Angular and NodeJS learning

2022.02.19. Angular practice

2022.02.18. Sport day

2022.02.17. NodeJS learning

2022.02.16. NodeJS and MySQL learning

2022.02.15. NodeJS learning

2022.02.14. NodeJS learning

2022.02.13. Angular practice

2022.02.12. Angular practice

2022.02.11. Sport day

2022.02.10. Angular practice

2022.02.09. PHP & Database learning

2022.02.08. Angular practice

2022.02.07. NodeJS and MySQL learning

2022.02.06. NodeJS and Express learning

2022.02.05. NodeJS learning

2022.02.04. Sport day

2022.02.03. JavaScript practice

2022.02.02. Database learning

2022.02.01. JavaScript practice

2022.01.31. JavaScript practice

2022.01.30. JavaScript practice

2022.01.29. JavaScript practice

2022.01.28. Sport day

2022.01.27. JavaScript practice

2022.01.26. PHP & Database learning

2022.01.25. JavaScript repetition

2022.01.24. MySQL learning

2022.01.23. JavaScript repetition

2022.01.22. JavaScript repetition

2022.01.21. JavaScript repetition

2022.01.20. Sport day

2022.01.19. MySQL learning

2022.01.18. Angular learning

2022.01.17. Angular learning

2022.01.16. Angular learning

2022.01.15. Angular learning

2022.01.14. Angular learning

2022.01.13. Sport day

2022.01.12. PHP learning

2022.01.11. Angular learning

2022.01.10. Database learning

2022.01.09. Angular learning

2022.01.08. Angular learning

2022.01.07. Angular learning

2022.01.06. Angular learning

2022.01.05. Sport day

2022.01.04. Angular learning

2022.01.03. Angular learning

2022.01.02. Angular learning

2022.01.01. Rest day

2021.12.31. Rest day

2021.12.30. Angular learning

2021.12.29. Angular learning

2021.12.28. Angular learning

2021.12.27. Sport day with short repetition

2021.12.26. Angular learning

2021.12.25. Rest day

2021.12.24. Rest day

2021.12.23. Angular learning

2021.12.22. Angular learning

2021.12.21. Sport day with short repetition

2021.12.20. Angular learning

2021.12.19. Angular learning

2021.12.18. Angular learning

2021.12.17. Sport day with short repetition

2021.12.16. Angular learning

2021.12.15. PHP and MySQL learning

2021.12.14. Angular learning

2021.12.13. Bootstrap repetition

2021.12.12. Angular learning

2021.12.11. Angular learning

2021.12.10. Angular learning

2021.12.09. Sport day with short repetition

2021.12.08. PHP and MySQL learning

2021.12.07. Sport day with short repetition

2021.12.06. Angular learning

2021.12.05. Sport day with short repetition

2021.12.04. Angular learning

2021.12.03. Sport day with short repetition

2021.12.02. Angular learning

2021.12.01. HTML, CSS, JS and Python practice

2021.11.30. Sport day with short repetition

2021.11.29. TypeScript learning

2021.11.28. Angular learning

2021.11.27. Angular learning

2021.11.26. Sport day with Angular learning

2021.11.25. Angular learning

2021.11.24. Angular and PHP learning

2021.11.23. Sport day with short repetition

2021.11.22. Python learning

2021.11.21. Angular learning

2021.11.20. Sport day with short repetition

2021.11.19. Sport day with short repetition

2021.11.18. Python learning

2021.11.17. PHP learning

2021.11.16. Angular learning

2021.11.15. Angular learning

2021.11.14. Angular learning

2021.11.13. Angular learning

2021.11.12. Sport day with short repetition

2021.11.11. Python and Angular learning

2021.11.10. PHP learning and HTML & CSS practice

2021.11.09. Sport day with short repetition

2021.11.08. Angular and JavaScript practice

2021.11.07. JavaScript practice

2021.11.06. Sport day with short repetition

2021.11.05. Sport day with short repetition

2021.11.04. Angular and Python learning

2021.11.03. HTML & CSS practice

2021.11.02. HTML & CSS practice

2021.11.01. Angular and NodeJS learning

2021.10.31. Angular and NodeJS learning

2021.10.30. Angular learning

2021.10.29. Sport day with short repetition

2021.10.28. Sport day with short repetition

2021.10.27. Angular learning

2021.10.26. Bootstrap learning

2021.10.25. Angular learning

2021.10.24. Angular learning

2021.10.23. Angular learning

2021.10.22. Sport day with short repetition

2021.10.21. JavaScript learning and HTML & Repetition

2021.10.20. Angular and PHP learning

2021.10.19. Angular learning

2021.10.18. JavaScript and Angular learning

2021.10.17. Sport day with short repetition

2021.10.16. Sport day with Angular learning

2021.10.15. Sport day with short repetition

2021.10.14. Angular learning and HTML & CSS practice

2021.10.13. Angular & Database learning

2021.10.12. Angular learning

2021.10.11. Python & local networks learning and JavaScript practice.

2021.10.10. Python learning

2021.10.09. JavaScript and Angular learning.

2021.10.08. Sport day with short Angular learning

2021.10.07. Javascript & Python learning and HTML & CSS practice

2021.10.06. Databases and Javascript learning

2021.10.05. Angular and Javascript learning

2021.10.04. Angular, CISCO Packet Tracer & JavaScript

2021.10.03. Angular learning & HTML and CSS repetition

2021.10.02. Sport day with repetition

2021.10.01. Sport day with repetition

2021.09.30. Angular learning & HTML and CSS repetition

2021.09.29. Databases and Angular learning

2021.09.28. Sport day with Angular learning

2021.09.27. Angular learning

2021.09.26. Sport day with JavaScript repetition

2021.09.25. Sport day with repetition

2021.09.24. WSL (Windows Subsystem for Linux) and NPM learning

2021.09.23. HTML, CSS & JavaScript learning

2021.09.22. JavaScript learning

2021.09.21. XAMPP phpMyAdmin, CISCO Networks and JavaScript learning

2021.09.20. XAMPP phpMyAdmin and JavaScript learning

2021.09.19. CISCO Networks and JavaScript learning

2021.09.18. Sport day with repetition

2021.09.17. Sport day with repetition

2021.09.16. HTML & CSS

2021.09.15. Databases

2021.09.14. Computer Technology Basics

2021.09.13. CISCO Networks

2021.09.12. HTML & CSS repetition

2021.09.11. HTML & CSS repetition

2021.09.10. Sport day with repetition

2021.09.09. Sport day with repetition

2021.09.08. JavaScript learning

2021.09.07. JavaScript learning

2021.09.06. JavaScript learning

2021.09.05. JavaScript learning

2021.09.04. Sport day with repetition

2021.09.03. Sport day with repetition

2021.08.29-09.02. Holiday with short HTML, CSS or JS repetition

2021.08.28. JavaScript learning

2021.08.27. Sport day with JavaScript repetition

2021.08.26. Sport day with JavaScript repetition

2021.08.25. JavaScript learning

2021.08.24. JavaScript learning

2021.08.23. JavaScript learning

2021.08.22. JavaScript learning

2021.08.21. JavaScript learning

2021.08.20. Sport day with JavaScript repetition

2021.08.19. Sport day with JavaScript repetition

2021.08.18. JavaScript learning

2021.08.17. JavaScript learning

2021.08.16. JavaScript learning

2021.08.15. JavaScript learning

2021.08.14. JavaScript learning

2021.08.13. Sport day with JavaScript repetition

2021.08.12. Sport day with JavaScript repetition

2021.07.26-08.11. Holiday with short HTML, CSS or JS repetition

2021.07.25. JavaScript learning

2021.07.24. JavaScript learning

2021.07.23. Sport day with JavaScript repetition

2021.07.22. Sport day with JavaScript repetition

2021.07.21. JavaScript learning

2021.07.20. JavaScript learning

2021.07.19. JavaScript learning

2021.07.18. JavaScript learning

2021.07.17. JavaScript learning

2021.07.16. Sport day with JavaScript repetition

2021.07.15. Sport day with JavaScript repetition

2021.07.14. JavaScript learning

2021.07.13. JavaScript learning

2021.07.12. JavaScript learning

2021.07.11. JavaScript learning

2021.07.10. JavaScript learning

2021.07.09. Sport day with JavaScript repetition

2021.07.08. Sport day with JavaScript repetition

2021.07.07. JavaScript learning

2021.07.06. JavaScript learning

2021.07.05. Rest day

2021.07.04. JavaScript learning

2021.07.03. Sport day with JavaScript repetition

2021.07.02. Sport day with JavaScript repetition

2021.07.01. JavaScript learning

2021.06.30. JavaScript learning

2021.06.29. JavaScript learning

2021.06.28. JavaScript learning

2021.06.27. JavaScript learning

2021.06.26. JavaScript practice

2021.06.25. Sport day with JavaScript repetition

2021.06.24. JavaScript learning

2021.06.23. Sport day with JavaScript repetition

2021.06.22. JavaScript learning

2021.06.21. JavaScript learning

2021.06.20. JavaScript learning

2021.06.19. JavaScript practice

2021.06.18. JavaScript learning

2021.06.17. Sport day with JavaScript repetition

2021.06.16. Sport day with JavaScript repetition

2021.06.15. JavaScript learning

2021.06.14. JavaScript learning

2021.06.13. JavaScript learning

2021.06.12. JavaScript learning

2021.06.11. Sport day with JavaScript repetition

2021.06.10. Sport day with JavaScript repetition

2021.06.09. Practicing HTML & CSS

2021.06.08. Practicing HTML & CSS

2021.06.07. Practicing HTML & CSS

2021.06.06. JavaScript learning

2021.06.05. JavaScript learning

2021.06.04. Sport day with JavaScript repetition

2021.06.03. Sport day with JavaScript repetition

2021.06.02. JavaScript learning

2021.06.01. JavaScript learning

2021.05.31. JavaScript learning

2021.05.30. JavaScript learning

2021.05.29. JavaScript learning

2021.05.28. Sport day with JavaScript practice

2021.05.27. Sport day with JavaScript learning

2021.05.26. Angular and JavaScript learning

2021.05.25. Angular learning

2021.05.24. Angular learning

2021.05.23. Angular learning

2021.05.22. Sport day with Angular practice

2021.05.21. Sport day with short Angular repetition

2021.05.20. Sport day with Angular learning

2021.05.19. Angular learning

2021.05.18. Angular learning

2021.05.17. Angular learning

2021.05.16. Angular learning

2021.05.15. Sport day with Angular practice

2021.05.14. Sport day with short Angular repetition

2021.05.13. Sport day with Angular learning

2021.05.12. Angular learning

2021.05.11. Angular learning

2021.05.10. Typescript learning

2021.05.09. Typescript learning

2021.05.08. Sport day with short repetition

2021.05.07. Sport day with short repetition

2021.05.06. Sport day with repetition

2021.05.05. JavaScript repetition

2021.05.04. CSS repetition ([Q&A and Notes](https://github.com/MrDanielHarka/learning/blob/main/css.md))

2021.05.03. Internet Basics repetition ([Q&A and Notes](https://github.com/MrDanielHarka/learning/blob/main/internet.md))

2021.05.02. Sport day with repetition

2021.05.01. Sport day with repetition

2021.04.30. Sport day with repetition

2021.04.29. Sport day with short repetition

2021.04.28. HTML & CSS practice

2021.04.27. JavaScript repetition

2021.04.26. HTML & CSS practice

2021.04.25. CSS practice ([Q&A and Notes](https://github.com/MrDanielHarka/learning/blob/main/css.md))

2021.04.24. Sport day with short repetition

2021.04.23. Sport day with short repetition

2021.04.22. Sport day with short repetition

2021.04.21. CSS practice ([Q&A and Notes](https://github.com/MrDanielHarka/learning/blob/main/css.md))

2021.04.20. JavaScript OOP repetition

2021.04.19. HTML and CSS repetition

2021.04.18. CSS repetition ([Q&A and Notes](https://github.com/MrDanielHarka/learning/blob/main/css.md))

2021.04.17. CSS repetition ([Q&A and Notes](https://github.com/MrDanielHarka/learning/blob/main/css.md))

2021.04.16. Sport day with short repetition

2021.04.15. Sport day with short repetition

2021.04.14. JavaScript and CSS practice

2021.04.13. JavaScript and CSS practice

2021.04.12. Repeating what I learned until now

2021.04.11. Debugging ([Q&A and Notes](https://github.com/MrDanielHarka/learning/blob/main/debugging.md)) & Repetition

2021.04.10. Git ([Q&A and Notes](https://github.com/MrDanielHarka/learning/blob/main/git.md)) & JavaScript

2021.04.09. Sport day with short repetition

2021.04.08. Sport day with short repetition

2021.04.07. Sport day with short repetition

2021.04.06. JavaScript Practice

2021.04.05. Reading Head First JavaScript

2021.04.04. JavaScript OOP and Classes

2021.04.03. JavaScript Basics Practice ([My JS CheatSheet](https://github.com/MrDanielHarka/cheat-sheets/blob/main/JavaScript-cheat-sheet.md))

2021.04.02. JavaScript Basics Practice ([My JS CheatSheet](https://github.com/MrDanielHarka/cheat-sheets/blob/main/JavaScript-cheat-sheet.md))

2021.04.01. Sport day with short repetition

2021.03.31. JavaScript Basics repetition ([My JS CheatSheet](https://github.com/MrDanielHarka/cheat-sheets/blob/main/JavaScript-cheat-sheet.md))

2021.03.30. Basic Node.js CLI ([Q&A and Notes](https://github.com/MrDanielHarka/learning/blob/main/node-js.md)) & OOP and Classes

2021.03.29. Internet Basics ([Q&A and Notes](https://github.com/MrDanielHarka/learning/blob/main/internet.md))

2021.03.28. Internet Basics ([Q&A and Notes](https://github.com/MrDanielHarka/learning/blob/main/internet.md))

2021.03.27. Algorithms ([Q&A and Notes](https://github.com/MrDanielHarka/learning/blob/main/algorithms.md))

2021.03.26. Data Structures ([Q&A and Notes](https://github.com/MrDanielHarka/learning/blob/main/data-structures.md))

2021.03.25. Sport day with short repetition

2021.03.24. Linux module ([Q&A and Notes](https://github.com/MrDanielHarka/learning/blob/main/linux.md))

2021.03.23. JavaScript Basics ([My JS CheatSheet](https://github.com/MrDanielHarka/cheat-sheets/blob/main/JavaScript-cheat-sheet.md))

2021.03.22. Sport day & Linux installation Added ([Q&A and Notes](https://github.com/MrDanielHarka/learning/blob/main/linux.md))

**What I learned before, but not documented here**\
A lot, but the following are worth mentioning:\
WordPress, HTML5, CSS, some VERY basic Python.
